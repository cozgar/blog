---
title: How do you manage your passwords?
date: 2020-12-09
tags: ["personal security"]
draft: true
---

## One password to rule them all?
- One password for everything or different passwords for every account
- How do you remember all of your passwords?
    - Writing them down
    - In a file on your computer
    - Saved to your browser
    - Using a password manager?

## Why should I use a password manager?
- One place to view, change, and use all account passwords
- Web browser integration options for ease-of-use
- Managing 



## Password manager examples

**Bitwarden**

**Lastpass**

**KeyPass**